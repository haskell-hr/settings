{-# LANGUAGE TemplateHaskell, QuasiQuotes, RecordWildCards, ViewPatterns, FlexibleInstances, LambdaCase #-}
{-# OPTIONS_GHC -Wno-orphans #-}
-- | To use the module, create a Settings module in your project and add a
--       embedSettings PATH_TO_SETTINGS_FILE
--   line. Requires Template Haskell
module Settings.Initialize where

import GHC.IO (unsafePerformIO)
import System.Environment
import System.Directory
import Language.Haskell.TH
import qualified Language.Haskell.TH.Syntax as TH
import Language.Haskell.TH.Syntax (addDependentFile)
import Data.Yaml.Aeson
import Data.Aeson.Lens
import qualified Data.HashMap.Strict as HM
import qualified Data.Text as Text
import Data.Scientific
import Configuration.Dotenv
import Data.FileEmbed
import Data.Foldable
import Data.Text (Text)
import Data.Char (toLower, toUpper)
import Data.String.Conv (toS)
import Control.Monad (when, void)
import Data.Function ((&))
import Control.Lens ((^?))

-- | Hack to let Text be read without quotation marks
class SettingsType a where
    settingFromString :: String -> a

instance SettingsType Integer where
    settingFromString = read

instance SettingsType Bool where
    settingFromString (fmap toLower -> "true") = True
    settingFromString (fmap toLower -> "false") = False
    settingFromString str = error $ "Cannot parse a bool from " <> str

instance SettingsType Double where
    settingFromString = read

instance SettingsType Text where
    settingFromString = toS

instance SettingsType [Text] where
    settingFromString = Text.splitOn "," . toS

whenM :: Monad m => m Bool -> m () -> m ()
whenM mbool a = mbool >>= \b -> when b a

{-# NOINLINE loadEnvWithDef #-}
loadEnvWithDef :: SettingsType a => Text -> a -> a
loadEnvWithDef env def = unsafePerformIO $ do
    whenM (doesFileExist ".env") (loadFile defaultConfig & void)
    maybe def settingFromString <$> lookupEnv (toS env)

embedSettings :: Text -> DecsQ
embedSettings path = do
    relPath <- makeRelativeToProject (toS path)
    file <- runIO (readFile relPath)
    settingsValue <- case decodeEither' (toS file) of
        Left err -> reportError "Failed to embed settings file" >> fail (show err)
        Right val -> return val
    addDependentFile (toS relPath)
    case settingsValue of
        Object hm -> do
            let pairs    = HM.toList hm
                allPairs = concatMap flatten pairs
            concat <$> mapM generateDeclaration allPairs
        i -> error $ "Failed to parse the settings file. Expected the topmost structure to be an \
                      \object but instead it was " ++ show i

mkNameT :: Text -> Name
mkNameT = mkName . toS

textLit :: Text -> Lit
textLit = stringL . toS

camel :: Text -> Text
camel t = head parts <> mconcat (fmap capitalizeFirst (tail parts))
    where parts = Text.splitOn "-" t

capitalizeFirst :: Text -> Text
capitalizeFirst t = toUpper (Text.head t) `Text.cons` Text.tail t

prependName :: Text -> (Text, Setting) -> (Text, Setting)
prependName pref (name, val) = (pref <> capitalizeFirst name, val)

data SettingValue = TextSetting Text | IntegerSetting Integer
                  | DoubleSetting Double | BoolSetting Bool | TextListSetting [Text]
                    deriving (Eq, Ord, Read, Show)

data Setting = Setting { defaultValue    :: SettingValue
                       , overrideOptions :: [(Text, Value)] }

withoutDefault :: [(Text, Value)] -> [(Text, Value)]
withoutDefault = filter ((/= "default") . fst)

toTextList :: (Functor f, Foldable f) => f Value -> [Text]
toTextList = toList . fmap (\case
    String s -> s
    _ -> error "Only lists of strings are valid settings values")

flatten :: (Text, Value) -> [(Text, Setting)]
flatten (name, Object obj) = case Object obj ^? key "default" of
    Just (String s) -> [(camel name, Setting (TextSetting s) overrds)]
    Just (Bool b) -> [(camel name, Setting (BoolSetting b) overrds)]
    Just (Number n) -> let val = case floatingOrInteger n of
                               Left  d -> DoubleSetting d
                               Right i -> IntegerSetting i
                       in [(camel name, Setting val overrds)]
    Just (Array l) -> [(camel name, Setting (TextListSetting (toTextList l)) overrds)]
    _               -> fmap (prependName name) (concatMap flatten (HM.toList obj))
    where
    overrds = withoutDefault (HM.toList obj)
flatten (name, _) = error $ toS $ "Setting " <> name <> " needs to be an object"

overrides :: [(Text, Value)] -> SettingValue -> ExpQ
overrides [] (TextSetting t) = litE (textLit t)
overrides [] (IntegerSetting i) = litE (integerL i)
overrides [] (DoubleSetting d) = litE (doublePrimL (realToFrac d))
overrides [] (BoolSetting b) = conE (if b then 'True else 'False)
overrides [] (TextListSetting l) = TH.lift l
overrides (("_env", String envVar) : rest) s =
    [e| $(varE 'loadEnvWithDef) $(litE (textLit envVar)) $(overrides rest s)|]
overrides ((o, _) : _) _ = error $ toS $ "Unsupported setting override " <> o

parseSetting :: Setting -> ExpQ
parseSetting Setting{..} = overrides overrideOptions defaultValue

generateDeclaration :: (Text, Setting) -> DecsQ
generateDeclaration (name, val) = do
    let name' = mkNameT name
        dType = case defaultValue val of
            TextSetting _     -> ConT ''Text
            IntegerSetting _  -> ConT ''Integer
            DoubleSetting _   -> ConT ''Double
            BoolSetting _     -> ConT ''Bool
            TextListSetting _ -> AppT ListT (ConT ''Text)
        sig   = SigD name' dType
    impl <- [d| $(varP (mkNameT name)) = $(parseSetting val) |]
    return (sig : impl)
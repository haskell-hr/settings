-- | This is roughly how the generated settings module would look
module Settings.Sample where

import Data.Text (Text)
import Settings.Initialize

-- static-dir:     "_env:STATIC_DIR:static"
-- host:           "_env:HOST:*4" # any IPv4 host
-- port:           "_env:PORT:3000" # NB: The port `yesod devel` uses is distinct from this value. Set the `yesod devel` port from the command line.
-- ip-from-header: "_env:IP_FROM_HEADER:false"
--
-- # Default behavior: determine the application root from the request headers.
-- # Uncomment to set an explicit approot
-- #approot:        "_env:APPROOT:http://localhost:3000"
--
-- # Optional values with the following production defaults.
-- # In development, they default to the inverse.
-- #
-- # development: false
-- # detailed-logging: false
-- # should-log-all: false
-- # reload-templates: false
-- # mutable-static: false
-- # skip-combining: false
--
-- # NB: If you need a numeric value (e.g. 123) to parse as a String, wrap it in single quotes (e.g. "_env:PGPASS:'123'")
-- # See https://github.com/yesodweb/yesod/wiki/Configuration#parsing-numeric-values-as-strings
--
-- database:
--   user:     "_env:PGUSER:postgres"
--   password: "_env:PGPASS:admin"
--   host:     "_env:PGHOST:localhost"
--   port:     "_env:PGPORT:5432"
--   database: "_env:PGDATABASE:yesod-web"
--   poolsize: "_env:PGPOOLSIZE:10"

staticDir :: Text
staticDir = loadEnvWithDef "STATIC_DIR" "static"

host :: Text
host = loadEnvWithDef "HOST" "4"

-- ...

databaseUser :: Text
databaseUser = loadEnvWithDef "PGUSER" "postgres"

databasePassword :: Text
databasePassword = loadEnvWithDef "PGPASS" "admin"
